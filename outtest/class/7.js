/*Support for static members.*/
var butterfly = (function () {
    function butterfly() {
        this.tribefound = 'Africa';
    }
    butterfly.showUnitWithPlace = function () {
        console.log(this.unit + 'found in ');
        //console.log(this.tribefound); // only static members accessible in static methods.
    };
    butterfly.unit = 3;
    return butterfly;
}());
console.log(butterfly.unit);
butterfly.showUnitWithPlace();
//console.log(butterfly.tribefound); //only static members accessible without instace.
