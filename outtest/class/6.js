/*Support getters and setters.*/
var clsGetSet = (function () {
    function clsGetSet(id) {
        this.id = id;
    }
    Object.defineProperty(clsGetSet.prototype, "Id", {
        get: function () {
            return this.id;
        },
        set: function (id) {
            this.id = id;
        },
        enumerable: true,
        configurable: true
    });
    return clsGetSet;
}());
var mygetset = new clsGetSet(5);
console.log(mygetset.Id);
mygetset.Id = 455;
console.log(mygetset.Id);
