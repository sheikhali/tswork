var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/*
Show use of
class, constructor, new, extends, super keyword.
Does not support multiple inheritance of classes out of the box but for interface.
Does not support constructor overloading out of the box.
*/
var class0 = (function () {
    function class0(oldname) {
        this.oldname = oldname;
        this.name0 = oldname;
    }
    ;
    return class0;
}());
var class1 = (function () {
    function class1(oldname) {
        this.oldname = oldname;
        this.name = oldname;
    }
    ;
    return class1;
}());
var class2 = (function (_super) {
    __extends(class2, _super);
    function class2(myname, make) {
        _super.call(this, myname);
        this.myname = myname;
        this.make = make;
    }
    class2.prototype.show = function () {
        console.log(this.make);
        console.log(this.name);
    };
    return class2;
}(class1));
var myclass = new class2("Orange", "Apple");
myclass.show();
