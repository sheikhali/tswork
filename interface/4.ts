/*interface function declarations*/
interface myfunc1{
        (name: string, location: string, pin : number):boolean
}

let f1 : myfunc1;
let f2 : myfunc1;

f1 = function(name : string, location: string, pin: number){
    return true;
}

f2 = function(tname : string, tlocation: string, tpin: number){
    return true;
}