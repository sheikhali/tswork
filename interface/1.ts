/*shows type inference - compiler only checks that at least the ones required are present and match the types required*/
function nointerface(personx : {name:string}){
    console.log(personx.name);
};
let person1 = {age : 33, name : 'P. V. Das'};
nointerface(person1);
