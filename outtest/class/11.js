/*Optional, ...rest and default parameters*/
var Person = (function () {
    function Person() {
    }
    Person.prototype.getMe = function (name, gender, location) {
        if (location === void 0) { location = 'U.S.'; }
        console.log(name + '/' + gender + '/' + location);
    };
    Person.prototype.showMe = function (name) {
        var Power = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            Power[_i - 1] = arguments[_i];
        }
        console.log(name);
        for (var x_1 in Power)
            console.log(Power[x_1]);
    };
    return Person;
}());
;
var pm = new Person();
pm.getMe('Rahul Sinha');
pm.showMe('Vikar', 'Ali', 3, false, 0x300);
