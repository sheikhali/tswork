/*interface with prop and methods*/
/*experiments with removing/changing return parameter*/
/*experiments with access modifiers*/
interface functionMixed{
    length : number;    
    show(name : string):void;
}

class cfirst implements functionMixed{
    public length : number;
    public show(name: string):void{
        console.log(name);
        //return true;
    }
}

