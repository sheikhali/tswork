"use strict";
var clsMasterlist = (function () {
    function clsMasterlist(mlist) {
        this.mlist = mlist;
    }
    ;
    clsMasterlist.prototype.showmaster = function () {
        for (var m in this.mlist) {
            console.log(this.mlist[m]);
        }
    };
    return clsMasterlist;
}());
exports.clsMasterlist = clsMasterlist;
exports.caps = ["A", "B", "C", "D"];
exports.small = ["a", "b", "c", "d"];
