/*Supports public, private and protected modifiers.*/

class masterOne {
    public pbName : string = 'x';
    private prName: string = 'y';
    protected ptName : string = 'z';

    public showNames(){
        console.log(this.pbName +'/master'); //ok
        console.log(this.ptName + '/master'); //ok
        console.log(this.prName + '/master');//ok
        
    }
}

class slaveOne extends masterOne{
    public showNames(){
        console.log(this.pbName +'/slave'); //ok
        console.log(this.ptName + '/slave'); //protected accessible in subclass
        //console.log(this.prName + '/');//private not accessible in subclass
    }
}

let myMaster = new masterOne();

myMaster.pbName = 'Ola Cabs'; // ok
//myMaster.prName --private not accessible in public space.
//myMaster.ptName --protected not available  in public space.

myMaster.showNames();

let mySlave = new slaveOne();
mySlave.showNames(); 