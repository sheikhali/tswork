/*shows type inference - compiler only checks that at least the ones required are present and match the types required*/
function nointerface(personx) {
    console.log(personx.name);
}
;
var person1 = { age: 33, name: 'P. V. Das' };
nointerface(person1);
