/*we didn’t have to explicitly say that the object we pass to printLabel implements this interface like we might have to in other languages.
Here, it’s only the shape that matters. type-checker does not require that these properties come in any sort of order*/
function showemployee(personx) {
    console.log(personx.fullage);
}
var emp1 = { lastname: 'alibaba', fullage: 35, salary: 390 };
showemployee(emp1);
