/*let is block scoped, can be used only after declaration, redeclaration not allowed, shadowing possible, */
//console.log(yourname);//can be used only after declaration
var yourname;
//let yourname: string; //redeclaration not allowed
//block scope, shadowing
{
    var mybrother = 'A. M. Sigh';
    {
        var mybrother_1 = 'B. G. Gupta';
        console.log(mybrother_1);
    }
    console.log(mybrother);
}
var mypi = 3.14;
console.log(mypi);
mypi = 324; //constants can not be redeclared.
