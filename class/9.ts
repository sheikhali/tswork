/*function overloading and optional parameters*/
class clsOload{
    public showx():void;
    public showx(name:string): void;
    public showx(name?:any ){
        if(name)
        console.log('Your called me Mr./Mrs.'+ name);
        else
        console.log('No paramerter show \n');
    };  


    public print(pname: string):void;
    public print(pname: number):void;
    public print(pname: boolean):void;
    public print(pname: string|number|boolean):void{
        if(typeof pname == 'number')
            console.log("Number\n");
        if(typeof pname == 'string')
            console.log("String\n");
        if(typeof pname == 'boolean')
            console.log("Boolean\n");
    }
}

let objLoad = new clsOload();

objLoad.showx();
objLoad.showx('Muasam Gupta');

objLoad.print('I');
objLoad.print(33);
objLoad.print(4==4);