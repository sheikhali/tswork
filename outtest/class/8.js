/**
Support for abstract class and abstrct methods.
*/
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var clsAbs = (function () {
    function clsAbs() {
    }
    clsAbs.prototype.otherMethod = function () {
        console.log("Defined in abstract class itself");
    };
    return clsAbs;
}());
//let objAbs = new claAbs(); // instance of abstract class can not be created.
var clsImpl = (function (_super) {
    __extends(clsImpl, _super);
    function clsImpl() {
        _super.apply(this, arguments);
    }
    clsImpl.prototype.show = function () {
        console.log("Show is implemented in derived class");
    };
    return clsImpl;
}(clsAbs));
var objImpl = new clsImpl();
objImpl.otherMethod();
objImpl.show();
