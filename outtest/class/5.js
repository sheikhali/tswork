/*Parameter properties.
Parameter properties let you create and initialize a member in one place. In Constructor
*/
var clsParaShow = (function () {
    function clsParaShow(pbName, prName, ptName) {
        this.pbName = pbName;
        this.prName = prName;
        this.ptName = ptName;
    }
    clsParaShow.prototype.showMyDetail = function () {
        console.log(this.pbName + '/master'); //ok
        console.log(this.ptName + '/master'); //ok
        console.log(this.prName + '/master'); //ok
    };
    return clsParaShow;
}());
var objps = new clsParaShow('A', 'B', 'C');
objps.showMyDetail();
