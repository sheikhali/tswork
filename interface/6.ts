/*A class can extends only a single class in Typescript*/
/*But an interface can extend other/multiple interface*/
/*class animal{
    name : string;
}

class mammal {
    type : string;
}

class tiger extends animal, mammal{

}*/


interface animal{
    name : string;
}

interface mammal {
    type : string;
}

interface tiger extends animal, mammal{

}