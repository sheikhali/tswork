/*let is block scoped, can be used only after declaration, redeclaration not allowed, shadowing possible, */

//console.log(yourname);//can be used only after declaration
let yourname: string;
//let yourname: string; //redeclaration not allowed


//block scope, shadowing
{
let mybrother : string= 'A. M. Sigh';

    {
        let mybrother :string = 'B. G. Gupta';
        console.log(mybrother);
    }
console.log(mybrother);
} 

const mypi: number = 3.14;
console.log(mypi);
mypi =324; //constants can not be redeclared.
