/*Polymorphism using interface.*/
var Employee = (function () {
    function Employee(name) {
        this.name = name;
    }
    ;
    Employee.prototype.show = function () {
        return "Employee Name is : " + this.name;
    };
    return Employee;
}());
var Student = (function () {
    function Student(name) {
        this.name = name;
    }
    ;
    Student.prototype.show = function () {
        return "Student Name is : " + this.name;
    };
    return Student;
}());
var ip;
ip = new Employee("Rajbeer");
console.log(ip.show());
ip = new Student("Vishal");
console.log(ip.show());
