var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/*We override methods in the base class with methods that are specialized for the subclass.
Take not of use of super to call subclass functions.
*/
var mybase = (function () {
    function mybase() {
    }
    mybase.prototype.showname = function (yourname) {
        console.log("this is from base class : " + yourname);
    };
    return mybase;
}());
var subclass = (function (_super) {
    __extends(subclass, _super);
    function subclass() {
        _super.apply(this, arguments);
    }
    subclass.prototype.showname = function (yourname) {
        console.log("this is from subclass : " + yourname);
        _super.prototype.showname.call(this, yourname);
    };
    return subclass;
}(mybase));
var subone = new subclass();
subone.showname("M. P. Sahu.");
