/*Support getters and setters.*/
class clsGetSet{
    constructor(private id:number){
    }

    get Id(){
        return this.id; 
    }

    set Id(id: number){
        this.id = id;
    }
}

let mygetset = new clsGetSet(5);
console.log(mygetset.Id);
mygetset.Id = 455;
console.log(mygetset.Id);