/**
Support for abstract class and abstrct methods.
*/

abstract class clsAbs{
    abstract show():void;
    public otherMethod(){
        console.log("Defined in abstract class itself");
    }
}

//let objAbs = new claAbs(); // instance of abstract class can not be created.
class clsImpl extends clsAbs{
    public show(){
        console.log("Show is implemented in derived class");
    }
}


let objImpl = new clsImpl();
objImpl.otherMethod();
objImpl.show();