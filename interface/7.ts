/*class extends and implemented*/
/*show enum, constructor, super, this*/

interface animala{
    namea : string;
    show():void;
}

interface mammala {
    typea : string;
}

class tigera {
    colora : xcolor 
    constructor(public colorax: xcolor){
        this.colora = colorax;
    }
}

enum xcolor {red, green};


class cub extends tigera implements animala, mammala {
    namea : string;
    typea : string;
    public constructor(public colora: xcolor){
        super(colora);/*remove this and see error "becoz constructor of derived class must have super keyword*/
    }

    public show(){
        console.log(this.colora);
        console.log(xcolor[this.colora]);
    }

}

let mycub = new cub(xcolor.red);
mycub.show();
