/*simple class with enum*/

enum breedenum {x=1, y, z=2}

class animalwithbreed {
    breed : breedenum;
    showbreed(){
        console.log(this.breed);
        console.log(breedenum[this.breed]);
    };
}

let dog: animalwithbreed;
dog = new animalwithbreed();
dog.breed = breedenum.y;
dog.showbreed(); //even is y is assinged to breed it is considered a z becoz of 2 is the value of value.

let cat = new animalwithbreed();
cat.breed = breedenum.z;
cat.showbreed();

