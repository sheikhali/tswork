/*Polymorphism using interface.*/

interface IPerson{
    show():string;
}

class Employee implements IPerson{
    constructor(public name:string){};
    public show(){
        return "Employee Name is : "+ this.name;
    }
}

class Student implements IPerson{
    constructor(public name:string){};
    public show(){
        return "Student Name is : "+ this.name;
    }
}

let ip: IPerson;

ip = new Employee("Rajbeer"); 
console.log(ip.show());
ip = new Student("Vishal");
console.log(ip.show());