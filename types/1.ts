/*Typescript types.*/

let myName: string;
let myGender : boolean;
let myAge : number;
let myCars : number[] ;
let myJobs : Array<string>;
let myGroup : any;

myName = 'Indian Soldier';
myGender = true;
myAge = 33;
myCars = [3,5,7];
myJobs = ["A",'B','C'];

myGroup = 3;
myGroup = 'hello';
myGroup = true;


