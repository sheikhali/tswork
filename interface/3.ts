interface personoption {
    name : string;
    age ? : number;    
}

function showperson(personx : personoption ){
    console.log(personx.name);
    if(personx.age)
        console.log(personx.age);
}

var personopt1 = {name : 'M. R. Singh'};
var personopt2 = {name : 'M. R. Singh', age : 3};

showperson(personopt1);
showperson(personopt2);