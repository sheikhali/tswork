/*we didn’t have to explicitly say that the object we pass to printLabel implements this interface like we might have to in other languages. 
Here, it’s only the shape that matters. type-checker does not require that these properties come in any sort of order*/

interface employee {
    lastname : string;
    fullage : number;
}

function showemployee(personx : employee){
    console.log(personx.fullage);
}

let emp1 = {lastname: 'alibaba', fullage:35, salary : 390};
showemployee(emp1);
