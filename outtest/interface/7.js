/*class extends and implemented*/
/*show enum, constructor, super, this*/
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var tigera = (function () {
    function tigera(colorax) {
        this.colorax = colorax;
        this.colora = colorax;
    }
    return tigera;
}());
var xcolor;
(function (xcolor) {
    xcolor[xcolor["red"] = 0] = "red";
    xcolor[xcolor["green"] = 1] = "green";
})(xcolor || (xcolor = {}));
;
var cub = (function (_super) {
    __extends(cub, _super);
    function cub(colora) {
        _super.call(this, colora); /*remove this and see error "becoz constructor of derived class must have super keyword*/
        this.colora = colora;
    }
    cub.prototype.show = function () {
        console.log(this.colora);
        console.log(xcolor[this.colora]);
    };
    return cub;
}(tigera));
var mycub = new cub(xcolor.red);
mycub.show();
