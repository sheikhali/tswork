/*Dynamic types using Any*/
function showmyname(args) {
    console.log(args + " is of type" + typeof args);
}
showmyname(1);
showmyname('A');
showmyname(true);
showmyname({ name: 'Rahul' });
/*Dynamic types using Generics*/
function showyourname(args) {
    return args;
}
console.log(showyourname('Ansul'));
console.log(showyourname(4));
console.log(showyourname(false));
