/*function overloading and optional parameters*/
var clsOload = (function () {
    function clsOload() {
    }
    clsOload.prototype.showx = function (name) {
        if (name)
            console.log('Your called me Mr./Mrs.' + name);
        else
            console.log('No paramerter show \n');
    };
    ;
    clsOload.prototype.print = function (pname) {
        if (typeof pname == 'number')
            console.log("Number\n");
        if (typeof pname == 'string')
            console.log("String\n");
        if (typeof pname == 'boolean')
            console.log("Boolean\n");
    };
    return clsOload;
}());
var objLoad = new clsOload();
objLoad.showx();
objLoad.showx('Muasam Gupta');
objLoad.print('I');
objLoad.print(33);
objLoad.print(4 == 4);
