/*Optional, ...rest and default parameters*/
class Person{
    public getMe(name:string, gender?:boolean, location:string='U.S.' ){
        console.log(name+'/' +gender+ '/' +location);      
    }
    public showMe(name:string, ...Power){
        console.log(name);
        for(let x in Power)
            console.log(Power[x]);
    }
};

let pm = new Person();
pm.getMe('Rahul Sinha');
pm.showMe('Vikar','Ali',3, false, 0x300);