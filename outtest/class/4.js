/*Supports public, private and protected modifiers.*/
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var masterOne = (function () {
    function masterOne() {
        this.pbName = 'x';
        this.prName = 'y';
        this.ptName = 'z';
    }
    masterOne.prototype.showNames = function () {
        console.log(this.pbName + '/master'); //ok
        console.log(this.ptName + '/master'); //ok
        console.log(this.prName + '/master'); //ok
    };
    return masterOne;
}());
var slaveOne = (function (_super) {
    __extends(slaveOne, _super);
    function slaveOne() {
        _super.apply(this, arguments);
    }
    slaveOne.prototype.showNames = function () {
        console.log(this.pbName + '/slave'); //ok
        console.log(this.ptName + '/slave'); //protected accessible in subclass
        //console.log(this.prName + '/');//private not accessible in subclass
    };
    return slaveOne;
}(masterOne));
var myMaster = new masterOne();
myMaster.pbName = 'Ola Cabs'; // ok
//myMaster.prName --private not accessible in public space.
//myMaster.ptName --protected not available  in public space.
myMaster.showNames();
var mySlave = new slaveOne();
mySlave.showNames();
