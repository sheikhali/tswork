/*We override methods in the base class with methods that are specialized for the subclass.
Take not of use of super to call subclass functions.
*/
class mybase{
    public showname(yourname: string){
        console.log("this is from base class : " + yourname);
    }
}

class subclass extends mybase{
    public showname(yourname:string){
        console.log("this is from subclass : " + yourname);
        super.showname(yourname);
    }
}

let subone = new subclass();
subone.showname("M. P. Sahu.");