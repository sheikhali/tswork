/*Dynamic types using Any*/
function showmyname(args?:any):any{
    console.log(args+ " is of type"+ typeof args);
}

showmyname(1);
showmyname('A');
showmyname(true);
showmyname({name:'Rahul'});


/*Dynamic types using Generics*/

function showyourname<T>(args:T):T{        
    return args;
}

console.log(showyourname<string>('Ansul'));
console.log(showyourname<number>(4));
console.log(showyourname<boolean>(false));
