/*
Show use of 
class, constructor, new, extends, super keyword.
Does not support multiple inheritance of classes out of the box but for interface.
Does not support constructor overloading out of the box.
*/
class class0{
    name0: string;
    public constructor(private oldname:string){
        this.name0 = oldname;
    };
}

class class1{
    name: string;
    public constructor(private oldname:string){
        this.name = oldname;
    };
}

class class2 extends class1/*, class0*//*multiple extend for class not supported*/ { 
    make:string;
    public constructor(private myname:string, make: string){
        super(myname);
        this.make = make;
    }

    public show(){
        console.log(this.make);
        console.log(this.name);        
    }

    /*public constructor(public arecode:number){
        //multiple constructor not allowed.
    }*/
}

let myclass = new class2("Orange","Apple");
myclass.show();




