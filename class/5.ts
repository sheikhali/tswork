/*Parameter properties.
Parameter properties let you create and initialize a member in one place. In Constructor
*/
class clsParaShow{
    constructor(public pbName: string, private prName:string, private ptName : string){
    }
    showMyDetail(){
        console.log(this.pbName +'/master'); //ok
        console.log(this.ptName + '/master'); //ok
        console.log(this.prName + '/master');//ok
    }
}

let objps = new clsParaShow('A','B','C');
objps.showMyDetail();