/*Support for static members.*/
class butterfly {
    public static unit : number = 3;
    public tribefound : string = 'Africa';

    public static showUnitWithPlace(){
        console.log(this.unit +'found in ');
        //console.log(this.tribefound); // only static members accessible in static methods.
    }
}

console.log(butterfly.unit);
butterfly.showUnitWithPlace();
//console.log(butterfly.tribefound); //only static members accessible without instace.

